<?php
require_once './pdo.php';
//Сортировка
$orderColumns = ['description', 'date_added', 'is_done'];
$orderColumn = $orderColumns[0];
$orderDirection = 0;

if (isset($_GET['order_column']) && isset($_GET['order_direction']) &&
    (int)$_GET['order_column'] < count($orderColumns) && (int)$_GET['order_direction'] <= 1) {
    $orderColumn = $orderColumns[(int)$_GET['order_column']];
    setcookie('order_column', $orderColumn);
    $orderDirection = (int)$_GET['order_direction'];
    setcookie('order_direction', $orderDirection);
} else {
    if (isset($_COOKIE['order_column']) && isset($_COOKIE['order_direction']) &&
        in_array($_COOKIE['order_column'], $orderColumns) && (int)$_COOKIE['order_direction'] <= 1
    ) {
        $orderColumn = $_COOKIE['order_column'];
        $orderDirection = $_COOKIE['order_direction'];
    }
}

$pdo = new PDO($pdo_string, $pdo_user, $pdo_pwd);
$success_message = '';
if (!empty($_POST['task'])) {
    if (empty($_POST['description'])) {
        $error_message = 'Описание задачи не может быть пустым!';
    } else {
        $args = [];
        if ($_POST['task'] == 'create') {
            $sql = 'INSERT INTO tasks (description, date_added) VALUES (:description, :date_added);';
            $args[':description'] = $_POST['description'];
            $args[':date_added'] = date('Y-m-d H:i:s');
            $error_message = '';
        } else {
            if (empty($_POST['id'])) {
                $error_message = 'Не указан ID записи';
            } else {
                $sql = 'UPDATE tasks SET description = :description WHERE id = :id;';
                $args[':description'] = $_POST['description'];
                $args[':id'] = $_POST['id'];
                $error_message = '';
            }
        }
        if (empty($error_message)) {
            $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute($args);
            $success_message = 'Задача успешно сохранена';
        }
    }
} else if (!empty($_GET['task'])) {
    if (empty($_GET['id'])) {
        $error_message = 'Не указан ID записи';
    } else {
        if ($_GET['task'] == 'delete') {
            $sql = 'DELETE FROM tasks WHERE id = :id;';
            $args[':id'] = $_GET['id'];
            $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute($args);
            $success_message = 'Задача успешно удалена';
        } else if ($_GET['task'] == 'finish') {
            $sql = 'UPDATE tasks SET is_done = 1 WHERE id = :id;';
            $args[':id'] = $_GET['id'];
            $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute($args);
            $success_message = 'Задача успешно обновлена';
        } else if ($_GET['task'] == 'edit') {
            $sql = 'SELECT description FROM tasks WHERE id = :id;';
            $args[':id'] = $_GET['id'];
            $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute($args);
            $result = $sth->fetch();
            if ($result === false) {
                $error_message = 'Неопределенная ошибка';
            } else {
                $task = 'update';
                $description = $result['description'];
                $id = $_GET['id'];
            }
        }
    }
}
?>

<html>
<head>
    <title>ToDo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <h3>Список дел</h3>
    <div class="page-header">

        <div class="jumbotron">
            <div class="row">
                <form action="index.php" method="post">
                    <?php
                    if (empty($task)) {
                        $description = '';
                        $task = 'create';
                        $task_name = 'Добавить';
                    } else {
                        echo "<input type=\"hidden\" name=\"id\" value=\"$id\">";
                        $task_name = 'Изменить';
                    }
                    echo "<input type=\"hidden\" name=\"task\" value=\"$task\">";
                    echo "<div class=\"col-md-9\"><input type=\"text\" class=\"form-control\" name=\"description\" placeholder=\"Текст\" value=\"$description\"></div>";
                    echo "<div class=\"col-md-3\"><input type=\"submit\" class=\"btn btn-primary\" name=\"submit\" value=\"$task_name\"> ";
                    if ($task == 'update') {
                        echo '<a href="index.php" class="btn btn-danger">Отменить</a>';
                    }
                    ?>
            </div>
            </form>
        </div>
    </div>

</div>
<div>
    <?php
    if (!empty($error_message)) {
        echo '<div class="alert alert-danger" role="alert">' . $error_message . '</div>';
    } elseif (!empty($success_message)) {
        echo '<div class="alert alert-success" role="alert">' . $success_message . '</div>';
    }
    $directionArrow = ($orderDirection == 0) ? 'asc' : 'desc';
    $headerText = "<i class=\"fa fa-fw fa-sort-" . (($orderDirection == 0) ? 'asc' : 'desc') . "\"></i>";
    ?>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th>
            <a href="index.php?order_column=0&order_direction=<?= ($orderColumn == 'description' && $orderDirection == 0) ? 1 : 0 ?>">
                Задание <?= ($orderColumn == 'description') ? ($headerText) : "" ?>
            </a>
        </th>
        <th>
            <a href="index.php?order_column=1&order_direction=<?= ($orderColumn == 'date_added' && $orderDirection == 0) ? 1 : 0 ?>">
                Время создания <?= ($orderColumn == 'date_added') ? ($headerText) : "" ?>
            </a>
        </th>
        <th>
            <a href="index.php?order_column=2&order_direction=<?= ($orderColumn == 'is_done' && $orderDirection == 0) ? 1 : 0 ?>">
                Состояние <?= ($orderColumn == 'is_done') ? ($headerText) : "" ?>
            </a>
        </th>
        <th>Операции</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sql = "SELECT id, description, is_done, date_added FROM tasks ORDER BY $orderColumn " .
        (($orderDirection == 0) ? 'ASC' : 'DESC' . ';');
    $sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

    $sth->execute();
    $result = $sth->fetchAll();
    foreach ($result as $row) {
        $class = ($row['is_done'] == 0) ? 'red' : 'green';
        $done = ($row['is_done'] == 0) ? 'Не выполнено' : 'Выполнено';
        echo "<tr><td class='$class'>" . $row['description'] . '</td><td>' . $row['date_added'] . '</td><td>' . $done . '</td>' .
            '<td><a href=\'index.php?task=finish&id=' . $row['id'] . '\' class=\'btn btn-success\'>Выполнено</a> <a href=\'index.php?task=edit&id=' . $row['id'] . '\' class=\'btn btn-info\'>Редактировать</a> <a href=\'index.php?task=delete&id=' . $row['id'] . '\' class=\'btn btn-danger\'>Удалить</a></td></tr>';
    }
    ?>
    </tbody>
</table>
<p>* Для сортировки данных таблицы кликните по заголовку столбца</p>
</body>
</html>
